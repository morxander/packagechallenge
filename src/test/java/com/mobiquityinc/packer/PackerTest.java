package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;

import static org.junit.jupiter.api.Assertions.*;

class PackerTest {

    @org.junit.jupiter.api.Test
    void pack() throws APIException {
        String filePath = "dummyFilePath";
        String algorithmSolution = "4\n-\n2,7\n8,9";
        assertEquals(Packer.pack(filePath), algorithmSolution);
    }
}