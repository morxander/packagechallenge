package com.mobiquityinc.packer;

import com.mobiquityinc.packer.model.ShippingItem;
import com.mobiquityinc.packer.model.ShippingPackage;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShippingPickerTest {

    @Test
    void max() {
        assertEquals(ShippingPicker.max(10, 100), 100);
    }

    @Test
    void getSelectItems() {
        assertEquals(ShippingPicker.getSelectItems(dataMock1()), "2,7");
        assertEquals(ShippingPicker.getSelectItems(dataMock2()), "-");
        assertEquals(ShippingPicker.getSelectItems(dataMock3()), "4");
    }

    ShippingPackage dataMock1() {
        List<ShippingItem> shippingItemList = new ArrayList<>();
        shippingItemList.add(0, new ShippingItem(1, 85310, 29));
        shippingItemList.add(1, new ShippingItem(2, 14550, 74));
        shippingItemList.add(2, new ShippingItem(3, 3980, 16));
        shippingItemList.add(3, new ShippingItem(4, 26240, 55));
        shippingItemList.add(4, new ShippingItem(5, 63690, 52));
        shippingItemList.add(5, new ShippingItem(6, 76250, 75));
        shippingItemList.add(6, new ShippingItem(7, 60020, 74));
        shippingItemList.add(7, new ShippingItem(8, 93180, 35));
        shippingItemList.add(8, new ShippingItem(9, 89950, 78));
        return new ShippingPackage(75000, shippingItemList);
    }

    ShippingPackage dataMock2() {
        List<ShippingItem> shippingItemList = new ArrayList<>();
        shippingItemList.add(0, new ShippingItem(1, 15300, 34));
        return new ShippingPackage(8000, shippingItemList);
    }

    ShippingPackage dataMock3() {
        List<ShippingItem> shippingItemList = new ArrayList<>();
        shippingItemList.add(0, new ShippingItem(1, 53380, 45));
        shippingItemList.add(0, new ShippingItem(2, 88620, 98));
        shippingItemList.add(0, new ShippingItem(3, 78480, 3));
        shippingItemList.add(0, new ShippingItem(4, 72300, 76));
        shippingItemList.add(0, new ShippingItem(5, 30180, 9));
        shippingItemList.add(0, new ShippingItem(6, 46340, 48));
        return new ShippingPackage(81000, shippingItemList);
    }
}