package com.mobiquityinc.exception;

public class APIException extends Exception {
    public APIException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
