package com.mobiquityinc.packer.io;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.ShippingItem;
import com.mobiquityinc.packer.model.ShippingPackage;

import java.util.ArrayList;
import java.util.List;

public class FileParser {

    /*
    * Since this function works with KG and usually
    * we break the KG to Grams and we don't break the Grams
    * and since the Dynamic Programming the best solution
    * So I am multiplying all the weights by 1000 ( Since the GK is 1000 G )
    * To convert all the weight to Integers so it can be usable for DP
    *  */
    public static ShippingPackage parseLine(String line) throws APIException {
        String[] lineParts = line.split(":");
        int weightLimit = Integer.valueOf(lineParts[0].trim());
        if(weightLimit > 100){
            throw new APIException("Package weight can't be bigger than 100", null);
        }
        String[] itemsStringArray = lineParts[1].trim().split(" ");
        if(itemsStringArray.length > 15){
            throw new APIException("Can't process more than 15 items", null);
        }
        List<ShippingItem> shippingItemList = new ArrayList<>();
        int index = 0;
        for(String item : itemsStringArray){
            item = item.replace("(", "");
            item = item.replace(")", "");
            item = item.replace("€", "");
            String[] itemDataArray = item.split(",");
            int itemIndex = Integer.valueOf(itemDataArray[0]);
            // Please read the comment above the function to understand the following line
            int itemWeight = (int) (Float.valueOf(itemDataArray[1]) * 1000);
            int itemPrice = Integer.valueOf(itemDataArray[2]);
            ShippingItem shippingItem = new ShippingItem(itemIndex, itemWeight, itemPrice);
            shippingItemList.add(index, shippingItem);
            index++;
        }
        // Please read the comment above the function to understand the following line
        return new ShippingPackage(weightLimit * 1000, shippingItemList);
    }
}
