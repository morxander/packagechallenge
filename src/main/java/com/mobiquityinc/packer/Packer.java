package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.io.FileParser;
import com.mobiquityinc.packer.io.FileReader;
import com.mobiquityinc.packer.model.ShippingPackage;

public class Packer {
    public static String pack(String filePath) throws APIException {
        // Reading  the file content
        String fileContent = FileReader.readFile(filePath);
        // Splitting by new lines with considering all OS new line characters.
        String[] fileLines = fileContent.split("\\r?\\n|\\r");
        for(String line :  fileLines){
            ShippingPackage shippingPackage = FileParser.parseLine(line);
            System.out.println(ShippingPicker.getSelectItems(shippingPackage));
        }
        return null;
    }
}
