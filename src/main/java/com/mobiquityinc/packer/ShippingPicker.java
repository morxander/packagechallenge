package com.mobiquityinc.packer;

import com.mobiquityinc.packer.model.ShippingItem;
import com.mobiquityinc.packer.model.ShippingPackage;

import java.util.List;

public class ShippingPicker {

    // Return the maximum value of 2 int vars
    static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    // Returns the selected item separated by comma
    static String getSelectItems(ShippingPackage shippingPackage) {
        List<ShippingItem> shippingItemList = shippingPackage.getShippingItemList();
        int weightLimit = shippingPackage.getWeightLimit();
        int n = shippingItemList.size();
        // Create a 2D Array to store the best price for every weight
        int K[][] = new int[n + 1][weightLimit + 1];
        // Filling the Array
        for (int i = 0; i <= n; i++) {
            for (int w = 0; w <= weightLimit; w++) {
                if (i == 0 || w == 0) {
                    K[i][w] = 0;
                } else if (shippingItemList.get(i - 1).getWeight() <= w) {
                    K[i][w] = max(shippingItemList.get(i - 1).getPrice() + K[i - 1][w - shippingItemList.get(i - 1).getWeight()], K[i - 1][w]);
                } else {
                    K[i][w] = K[i - 1][w];
                }
            }
        }
        // Determine the selected items
        int remainingPrice = K[n][weightLimit];
        int remainingWeight = weightLimit;
        String selectedItems = "-";
        for (int i = n; i > 0 && remainingPrice > 0; i--) {
            if (remainingPrice == 0)
                break;
            if (remainingPrice == K[i - 1][remainingWeight]) {
                continue;
            } else {
                ShippingItem item = shippingItemList.get(i - 1);
                if (selectedItems.equals("-")) {
                    selectedItems = String.valueOf(item.getIndex());
                } else {
                    selectedItems = item.getIndex() + "," + selectedItems;
                }
                remainingPrice -= item.getPrice();
                remainingWeight -= item.getWeight();
            }

        }
        return selectedItems;
    }
}