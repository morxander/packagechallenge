package com.mobiquityinc.packer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ShippingItem {
    private int index;
    private int weight;
    private int price;
}
