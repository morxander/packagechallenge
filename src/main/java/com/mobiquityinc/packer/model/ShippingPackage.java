package com.mobiquityinc.packer.model;

import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class ShippingPackage {
    private int weightLimit;
    private List<ShippingItem> shippingItemList;
}
